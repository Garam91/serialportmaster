#include "SerialComm.h"

/**************************************************************************
 * SerialComm class function
 * Function : 1.COM* port connect/disconnect
 *            2.Send and receive data
 *              (type:char,byte,int16_t->method overloading)
**************************************************************************/
SerialComm::SerialComm() {}
SerialComm::~SerialComm() {}

/*Connect serial port*/
bool SerialComm::Connect(const wchar_t* port_name, DWORD baud_rate,
                          BYTE data_bits, BYTE parity, BYTE stop_bits)
{
    if (!serial.Open(port_name)) {
        cout << "Connect Faliled" << endl;
        return false;
    }

    serial.Configure(baud_rate, data_bits, parity, stop_bits);
    serial.SetTimeouts(0, 0, 1, 0, 0);

    return true;
}

/*Disconnect serial port*/
void SerialComm::Disconnect()
{
    serial.Close();
}

/*Send data(overloading)*/
bool SerialComm::Send(char* data, UINT size)
{
    if (serial.WriteData(data, size))
        return true;
    else
        return false;
}

/*Send data(overloading)*/
bool SerialComm::Send(const char* data, UINT size)
{
    if (serial.WriteData(data, size))
        return true;
    else
        return false;
}

/*Send data(overloading)*/
bool SerialComm::Send(BYTE *data, UINT size)
{
    if (serial.WriteData(data, size))
        return true;
    else
        return false;
}

/*Receive data(overloading)*/
bool SerialComm::Receive(char* data, UINT size)
{
    if (serial.ReadData(data, size) == true)
        return true;
    else
        return false;
}

/*Receive data(overloading)*/
bool SerialComm::Receive(BYTE* data, UINT size)
{
    if (serial.ReadData(data, size) == true)
        return true;
    else
        return false;
}
