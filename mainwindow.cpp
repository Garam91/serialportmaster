#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <iostream>
#include <string>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->textEdit_Port->setText("COM");

    cr = "\r";
    lf = "\n";

    receive = new QTimer;
    connect(receive, SIGNAL(timeout()), this, SLOT(Receive()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_open_clicked()
{
    /*Setting value of port
     * port : port number(COM*)
     * baud_rate : baud rate, communication speed(bps)
     * byte_size : data size(bit)
     * parity : parity bit
     * stop bit : stop bit*/
    std::wstring port = ui->textEdit_Port->toPlainText().toStdWString();
    DWORD buad_rate = ui->comboBox_BaudRate->currentText().toULong();
    BYTE byte_size = static_cast<BYTE>(ui->comboBox_DataBits->currentText().toUShort());
    BYTE parity = static_cast<BYTE>(ui->comboBox_Parity->currentIndex());
    int stop_bit_index= ui->comboBox_StopBit->currentIndex();
    BYTE stop_bit = 0;
    switch (stop_bit_index) {
    case 0:stop_bit = ONESTOPBIT;break;
    case 1:stop_bit = TWOSTOPBITS;break;
    }

    /*Connect Serial Port*/
    bool connect = com.Connect(port.c_str(), buad_rate, byte_size, parity, stop_bit);
    if(connect == true){
        receive->start(1);

        ui->pushButton_close->setEnabled(true);
        ui->pushButton_open->setEnabled(false);

        /*Print configuration value of serial port*/
        std::wcout<<"Port number : "<<port<<std::endl;
        std::cout<<"Baud rate : "<<static_cast<int>(buad_rate)<<std::endl;
        std::cout<<"Data bits : "<<static_cast<int>(byte_size)<<std::endl;
        std::cout<<"Stop bit : "<<static_cast<int>(stop_bit)<<std::endl;
        std::cout<<"Parity : "<<static_cast<int>(parity)<<std::endl;
    }
}

void MainWindow::on_pushButton_close_clicked()
{
    /*Disconnect Serial Port*/
    com.Disconnect();
    ui->pushButton_close->setEnabled(false);
    ui->pushButton_open->setEnabled(true);
}

void MainWindow::Receive()
{
    char receive_data;
    bool receive = false;

    receive = com.Receive(&receive_data, 1);
    if(receive == true){
        QString text = QString(receive_data);
        ui->textBrowser_Receive->insertPlainText(text);
    }
}

void MainWindow::on_pushButton_Send1_clicked()
{
    /*Get data from text edit*/
   std::string send_data = ui->textEdit_Send1->toPlainText().toStdString();
   UINT data_size = static_cast<UINT>(send_data.length());

   if(ui->radioButton_CR->isChecked()){
       send_data += cr;
       data_size++;
   }
   if(ui->radioButton_LF->isChecked()){
       send_data += lf;
       data_size++;
   }
   if(ui->radioButton_CR_LF->isChecked()){
       send_data += cr+lf;
       data_size += 2;
   }

   bool send = com.Send(send_data.c_str(), data_size);
   if(send == true){
       std::cout<<"Send ok"<<std::endl;
   }
}

void MainWindow::on_pushButton_Send2_clicked()
{
    std::string send_data = ui->textEdit_Send2->toPlainText().toStdString();
    UINT data_size = static_cast<UINT>(send_data.length());

    if(ui->radioButton_CR->isChecked()){
        send_data += cr;
        data_size++;
    }
    if(ui->radioButton_LF->isChecked()){
        send_data += lf;
        data_size++;
    }
    if(ui->radioButton_CR_LF->isChecked()){
        send_data += cr+lf;
        data_size += 2;
    }

    bool send = com.Send(send_data.c_str(), data_size);
    if(send == true){
        std::cout<<"Send ok"<<std::endl;
    }
}

void MainWindow::on_pushButton_Send3_clicked()
{
    std::string send_data = ui->textEdit_Send3->toPlainText().toStdString();
    UINT data_size = static_cast<UINT>(send_data.length());

    if(ui->radioButton_CR->isChecked()){
        send_data += cr;
        data_size++;
    }
    if(ui->radioButton_LF->isChecked()){
        send_data += lf;
        data_size++;
    }
    if(ui->radioButton_CR_LF->isChecked()){
        send_data += cr+lf;
        data_size += 2;
    }

    bool send = com.Send(send_data.c_str(), data_size);
    if(send == true){
        std::cout<<"Send ok"<<std::endl;
    }
}

void MainWindow::on_pushButton_Send4_clicked()
{
    std::string send_data = ui->textEdit_Send4->toPlainText().toStdString();
    UINT data_size = static_cast<UINT>(send_data.length());

    if(ui->radioButton_CR->isChecked()){
        send_data += cr;
        data_size++;
    }
    if(ui->radioButton_LF->isChecked()){
        send_data += lf;
        data_size++;
    }
    if(ui->radioButton_CR_LF->isChecked()){
        send_data += cr+lf;
        data_size += 2;
    }

    bool send = com.Send(send_data.c_str(), data_size);
    if(send == true){
        std::cout<<"Send ok"<<std::endl;
    }
}
