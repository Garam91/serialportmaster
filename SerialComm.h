#ifndef SERIALCOMM_H
#define SERIALCOMM_H

#include "SerialPort.h"

class SerialComm
{
public:
    SerialComm();
    ~SerialComm();

    SerialPort serial;

    bool Connect(const wchar_t* port_name, DWORD baud_rate,
                 BYTE data_bits = 8, BYTE parity = NOPARITY,
                 BYTE stop_bits = ONESTOPBIT);
    void Disconnect();
    bool Send(char* data, UINT size);
    bool Send(const char* data, UINT size);
    bool Send(BYTE* data, UINT size);
    bool Receive(char* data, UINT size);
    bool Receive(BYTE* data, UINT size);
};
#endif // SERIALCOMM_H
