#include "SerialPort.h"

/**************************************************************************
 * SerialPort class function
 * Function : 1.Serial port open/close
 *            2.write and read data
 *              (type:char,byte,int16_t->method overloading)
**************************************************************************/
SerialPort::SerialPort()
{
    dcb.fBinary = true;
    dcb.fDsrSensitivity = false;
    dcb.fParity = FALSE;
    dcb.fOutX = false;
    dcb.fInX = false;
    dcb.fNull = false;
    dcb.fAbortOnError = true;
    dcb.fOutxCtsFlow = false;
    dcb.fOutxDsrFlow = false;
    dcb.fDtrControl = DTR_CONTROL_DISABLE;
    dcb.fDsrSensitivity = false;
    dcb.fRtsControl = RTS_CONTROL_DISABLE;
    dcb.fOutxCtsFlow = false;
    dcb.fOutxCtsFlow = false;
}

SerialPort::~SerialPort() {}

/*Open serial port*/
bool SerialPort::Open(std::wstring port_name)
{
    port_name = L"//./" + port_name;
    port_handle = CreateFile(port_name.c_str(),
                             GENERIC_READ | GENERIC_WRITE,
                             0, nullptr, OPEN_EXISTING, 0, nullptr);

    if (port_handle == INVALID_HANDLE_VALUE)
        return false;
    else
        return true;
}

/*Close serial port*/
void SerialPort::Close()
{
    CloseHandle(port_handle);
    return;
}

/*Configuration of port
 * baud rate, data size, parity, stop bit
*/
bool SerialPort::Configure(DWORD baud_rate, BYTE byte_size, BYTE parity,
                            BYTE stop_bits)
{
    if ((comm_state = GetCommState(port_handle, &dcb)) == 0){
        printf("\nGetCommState Error\n");
        CloseHandle(port_handle);
        return false;
    }

    dcb.BaudRate = baud_rate;
    dcb.ByteSize = byte_size;
    dcb.Parity = parity;
    dcb.StopBits = stop_bits;

    comm_state = SetCommState(port_handle, &dcb);

    if (comm_state == false){
        printf("SetCommState Error");
        CloseHandle(port_handle);
        return false;
    }

    return true;
}

/*Set timeout of read and write*/
bool SerialPort::SetTimeouts(DWORD read_intervaltimeout,
                              DWORD read_totaltimeout_constant,
                              DWORD read_totaltimeout_multiplier,
                              DWORD write_totaltimeout_constant,
                              DWORD write_totaltimeout_multiplier)
{
    if ((comm_state = GetCommTimeouts(port_handle, &comm_timeout)) == 0)
        return false;

    comm_timeout.ReadIntervalTimeout = read_intervaltimeout;
    comm_timeout.ReadTotalTimeoutConstant = read_totaltimeout_constant;
    comm_timeout.ReadTotalTimeoutMultiplier = read_totaltimeout_multiplier;
    comm_timeout.WriteTotalTimeoutConstant = write_totaltimeout_constant;
    comm_timeout.WriteTotalTimeoutMultiplier = write_totaltimeout_multiplier;

    comm_state = SetCommTimeouts(port_handle, &comm_timeout);

    if (comm_state == 0){
        printf("\nStCommTimeouts function failed\n");
        CloseHandle(port_handle);
        return false;
    }

    return true;
}

/*Write data(overloading)*/
bool SerialPort::WriteData(char* data, UINT size)
{
    bytes_written = 0;

    if (WriteFile(port_handle, data, size, &bytes_written, nullptr) == 0)
        return false;
    else
        return true;
}

/*Write data(overloading)*/
bool SerialPort::WriteData(const char* data, UINT size)
{
    bytes_written = 0;

    if (WriteFile(port_handle, data, size, &bytes_written, nullptr) == 0)
        return false;
    else
        return true;
}

/*Write data(overloading)*/
bool SerialPort::WriteData(BYTE* data, UINT size)
{
    bytes_written = 0;

    if (WriteFile(port_handle, data, size, &bytes_written, nullptr) == 0)
        return false;
    else
        return true;
}

/*Read data(overloading)*/
bool SerialPort::ReadData(char* data, UINT size)
{
    DWORD dwBytesTransferred = 0;

    if (ReadFile(port_handle, data, size, &dwBytesTransferred, nullptr)){
        if (dwBytesTransferred == size)
            return true;
    }

    return false;
}

/*Read data(overloading)*/
bool SerialPort::ReadData(BYTE* data, UINT size)
{
    DWORD dwBytesTransferred = 0;

    if (ReadFile(port_handle, data, size, &dwBytesTransferred, nullptr)){
        if (dwBytesTransferred == size)
            return true;
    }

    return false;
}
