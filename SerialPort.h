#ifndef SERIALPORT_H
#define SERIALPORT_H

#include <Windows.h>
#include <atlstr.h>
#include <iostream>

using namespace std;

class SerialPort
{
private:
    HANDLE  port_handle;
    DCB     dcb;
    COMMTIMEOUTS comm_timeout;
    bool    comm_state;
    DWORD   bytes_written;

public:
    SerialPort(void);
    virtual ~SerialPort(void);
    bool Open(std::wstring port_name);
    void Close();
    bool Configure(DWORD baud_rate, BYTE byte_size, BYTE parity,
                   BYTE stop_bits);
    bool SetTimeouts(DWORD read_intervaltimeout,
                     DWORD read_totaltimeout_constant,
                     DWORD read_totaltimeout_multiplier,
                     DWORD write_totaltimeout_constant,
                     DWORD write_totaltimeout_multiplier);
    bool WriteData(char* data, UINT size);
    bool WriteData(const char* data, UINT size);
    bool WriteData(BYTE* data, UINT size);
    bool ReadData(char* data, UINT size);
    bool ReadData(BYTE* data, UINT size);
};

#endif // SERIALPORT_H
