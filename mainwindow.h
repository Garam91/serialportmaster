#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>

#include "SerialComm.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_open_clicked();
    void on_pushButton_close_clicked();

    void Receive();
    void on_pushButton_Send1_clicked();
    void on_pushButton_Send2_clicked();
    void on_pushButton_Send3_clicked();
    void on_pushButton_Send4_clicked();

private:
    Ui::MainWindow *ui;

    SerialComm com;
    QTimer* receive;
    std::string cr, lf;
};

#endif // MAINWINDOW_H
